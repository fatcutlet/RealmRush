﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {
    [SerializeField] private int _towerLimit;
    [SerializeField] private GameObject _tower;
    [SerializeField] private Transform _towerRoot;

    private Queue<GameObject> _towers;

    public void AddTower(Waypoint waypoint)
    {

        //init queue of towers
        if(_towers == null)
        {
            _towers = new Queue<GameObject>(_towerLimit);
        }

        if(_towers.Count >= _towerLimit)
        {
            MoveExistingTower(waypoint);
            return;
        }

        InstantinateNewTower(waypoint);
    }

    private void MoveExistingTower(Waypoint waypoint)
    {
        var tomove = _towers.Dequeue();

        tomove.GetComponent<Tower>().PlacedWaypoint = waypoint;

        _towers.Enqueue(tomove);
    }

    private void InstantinateNewTower(Waypoint waypoint)
    {
        var tower = Instantiate(_tower, _towerRoot);

        tower.GetComponent<Tower>().PlacedWaypoint = waypoint;

        _towers.Enqueue(tower);
    }
}
