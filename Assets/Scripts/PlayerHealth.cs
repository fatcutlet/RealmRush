﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    [SerializeField] private int _playerHealth = 10;
    [SerializeField] private Text _healthText;
    [SerializeField] private AudioClip _damageSfx;

    private void Start()
    {
        UpdateUI();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            _playerHealth--;
            UpdateUI();
            GetComponent<AudioSource>().PlayOneShot(_damageSfx);
        }
    }

    private void UpdateUI()
    {
        _healthText.text = _playerHealth.ToString();
    }


}
