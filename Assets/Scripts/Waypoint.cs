﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    [SerializeField] private Color _exploredColor;

    private const int _gridSize = 10;
    private Vector2Int _gridPosition;
    private bool _isPlaceable = true;

    public bool IsExplored { get; set; }
    public Waypoint ExploredFrom { get; set; }
    public int GridSize
    {
        get
        {
            return _gridSize;
        }
    }
    public bool IsPlaceable
    {
        get
        {
            return _isPlaceable;
        }
        set
        {
            _isPlaceable = value;
        }
    }

    private void OnMouseOver()
    {
        if (!Input.GetMouseButtonDown(0))
        {
            return;
        }

        if (!IsPlaceable)
        {
            print("Can't place at " + GetGridPosition().x + ", " + GetGridPosition().y);
            return;
        }

        var factory = FindObjectOfType<TowerFactory>();
        factory.AddTower(this);        
        
    }

    public Vector2Int GetGridPosition()
    {
        return new Vector2Int(
            Mathf.RoundToInt(transform.position.x / GridSize),
            Mathf.RoundToInt(transform.position.z / GridSize)
            );
    }

    public void SetTopColor(Color color)
    {
        var topMesh = transform.Find("Top").GetComponent<MeshRenderer>();
        topMesh.material.color = color;
    }
}
