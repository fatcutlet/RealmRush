﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsEmitter : MonoBehaviour {

    [SerializeField] GameObject _bullets;

    public bool IsShooting {
        get
        {
            if(_bulletsParticle != null)
            {
                return _bulletsParticle.isPlaying;
            }
            else
            {
                return false;
            }
            
        }
    }


    private ParticleSystem _bulletsParticle;

    // Use this for initialization
    void Start () {
        var binstance =
            Instantiate(_bullets, gameObject.transform.position, Quaternion.identity, gameObject.transform);
        _bulletsParticle = binstance.GetComponent<ParticleSystem>();
        _bulletsParticle.Stop();
    }

    public void StartShooting()
    {
        _bulletsParticle.Play();
       // GetComponent<AudioSource>().Play();
    }

    public void StopShooting()
    {
        _bulletsParticle.Stop();
       // GetComponent<AudioSource>().Stop();
    }

}
