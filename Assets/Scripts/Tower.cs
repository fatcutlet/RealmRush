﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    [SerializeField] private Transform _objToPan;
    [SerializeField] private Transform _target;
    [SerializeField] private const float _towerYCoefficient = 5f;
    private List<Transform> _objectsToShoot = new List<Transform>();
    private BulletsEmitter _emiter;
    private Waypoint _waypoint;

    public Waypoint PlacedWaypoint
    {
        get
        {
            return _waypoint;
        }
        set
        {
            if(_waypoint != null)
            {
                _waypoint.IsPlaceable = true;
            }

            _waypoint = value;

            PlaceTower(_waypoint);

            _waypoint.IsPlaceable = false;

        }
    }

    public int Test { get; set; }

    private void Start()
    {
        _emiter = GetComponentInChildren<BulletsEmitter>();
    }

    void Update()
    {
        SwitchTarget();

        if (_target == null && _emiter.IsShooting)
        {
            StopShoot();
        }

        _objToPan.LookAt(_target);
    }

    private void SwitchTarget()
    {
        if (_target == null && _objectsToShoot.Count > 0)
        {
            while (_objectsToShoot.Count > 0)
            {
                _target = _objectsToShoot[0];
                _objectsToShoot.RemoveAt(0);

                if (_target != null)
                {
                    break;
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Enemy")
        {
            return;
        }

        var enemyt = other.transform;

        if (_target != null)
        {
            _objectsToShoot.Add(enemyt);
            return;
        }

        _target = enemyt;
        _emiter.StartShooting();

    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag != "Enemy")
        {
            return;
        }

        var enemyt = other.transform;

        if (_objectsToShoot.Contains(enemyt))
        {
            _objectsToShoot.Remove(enemyt);
        }

        if (_target != enemyt)
        {
            return;
        }

        if (_objectsToShoot.Count <= 0)
        {
            _target = null;
            StopShoot();
            return;
        }

        _target = _objectsToShoot[0];
    }

    private void StopShoot()
    {
        _emiter.StopShooting();
        _objToPan.rotation = Quaternion.identity;
    }

    private void PlaceTower(Waypoint waypoint)
    {
        var p = waypoint.gameObject.transform.position;
        transform.position = new Vector3(p.x, p.y + _towerYCoefficient, p.z);
        transform.rotation = Quaternion.identity;
    }
}
