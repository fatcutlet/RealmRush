﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {

    [SerializeField] private float _movementPerion = .5f;

    // Use this for initialization
    void Start () {
        Pathfinder pathfinder = FindObjectOfType<Pathfinder>();
        var path = pathfinder.GetPath();
        StartCoroutine(FollowPath(path));
    }
    
    // Update is called once per frame
    void Update () {
        
    }

    private IEnumerator FollowPath(List<Waypoint> path)
    {

        foreach (var point in path)
        {
            transform.position = point.transform.localPosition;

            yield return new WaitForSeconds(_movementPerion);
        }

        var suicide = GetComponent<SuicideDamage>();
        suicide.DoSuicideExplosion();

    }
}
