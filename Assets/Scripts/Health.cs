﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    [SerializeField] private int _health = 10;
    [SerializeField] private GameObject _deathVFX;
    [SerializeField] private GameObject _mainGameObj;
    [SerializeField] private GameObject _onHitVFX;
    [SerializeField] private AudioClip _onHitSFX;
    [SerializeField] private AudioClip _onDeathSFX;


    private ParticleSystem _hitParticle;

    private void Start()
    {
        var go = Instantiate(_onHitVFX,
            gameObject.transform);
        _hitParticle = go.GetComponent<ParticleSystem>();
    }


    public void ApplyDamage(int damage)
    {
        _health = _health - damage;

        GetComponent<AudioSource>().PlayOneShot(_onHitSFX);

        if (_health <= 0)
        {
            Death();
        }
        else
        {
            _hitParticle.Play();
        }
    }

    private void Death()
    {
        Instantiate(_deathVFX, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(_onDeathSFX, Camera.main.transform.position);

        Destroy(_mainGameObj);
    }

}
