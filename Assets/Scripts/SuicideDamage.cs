﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuicideDamage : MonoBehaviour {

    [SerializeField] GameObject _suicideVFX;
    [SerializeField] Vector3 _offset = new Vector3(0f, 5f, 0f);

    public void DoSuicideExplosion()
    {
        Instantiate(_suicideVFX, transform.position + _offset, Quaternion.identity);

        Destroy(gameObject);
    }
}
