﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour {

    [SerializeField] Waypoint _start, _end, _center;

    private Dictionary<Vector2Int, Waypoint> _grid;
    Queue<Waypoint> _queue = new Queue<Waypoint>();
    [SerializeField] private bool _isRunning = true;

    private readonly Vector2Int[] _directions =
    {
       Vector2Int.up,
       Vector2Int.right,
       Vector2Int.left,
       Vector2Int.down
    };

    private List<Waypoint> _path; //TODO make private

    #region Unity Messages
    #endregion

    private void CratePath()
    {
        _path = new List<Waypoint>();
        SetAsPath(_end);

        var previous = _end.ExploredFrom;
        while (previous != _start)
        {
            SetAsPath(previous);
            previous = previous.ExploredFrom;
            
        }

        SetAsPath(_start);

        _path.Reverse();
    }

    private void SetAsPath(Waypoint waypoint)
    {
        _path.Add(waypoint);
        waypoint.IsPlaceable = false;
    }


    #region Path Finding
    private void BreadthFirstSearch()
    {
        _queue.Enqueue(_start);
        while(_queue.Count > 0 && _isRunning)
        {
            _center = _queue.Dequeue();
            HaltIfEndFound(_center);
            ExploreNighbours();
            _center.IsExplored = true;
        }

        print("Finishing pathfinding?"); //TODO remove
    }

    private void HaltIfEndFound(Waypoint center)
    {
        if (_end == center)
        {
            _isRunning = false;
        }
    }

    private void ExploreNighbours()
    {
        if(!_isRunning) { return; }

        foreach (var direction in _directions)
        {
            var nbrCoordinates = _center.GetGridPosition() + direction;
            if(_grid.ContainsKey(nbrCoordinates))
            {
                QueueNewNeighbours(nbrCoordinates);
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int nbrCoordinates)
    {
        var neigbour = _grid[nbrCoordinates];

        if (neigbour.IsExplored || _queue.Contains(neigbour))
            { return; }

        _queue.Enqueue(neigbour);
        neigbour.ExploredFrom = _center;
    }
    #endregion

    private void LoadBlocks()
    {
        _grid = new Dictionary<Vector2Int, Waypoint>();
        var waypoints = FindObjectsOfType<Waypoint>();
        foreach(var p in waypoints)
        {
            var pos = p.GetGridPosition();
            if (_grid.ContainsKey(pos))
            {
                Debug.LogWarning("Overlaping block: " + p);
            }
            else
            {
                _grid.Add(pos, p);
            }
        }
        print("Loaded " + _grid.Count + " blocks");
    }

    private void StartEndColorize()
    {
        _start.SetTopColor(Color.cyan);
        _end.SetTopColor(Color.magenta);
    }

    public List<Waypoint> GetPath()
    {
        if(_path == null)
        {
            CalculatePath();
        }

        return _path;
    }

    private void CalculatePath()
    {
        LoadBlocks();
        StartEndColorize();
        BreadthFirstSearch();
        CratePath();
    }
}
