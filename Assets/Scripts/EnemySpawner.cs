﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    [SerializeField] private GameObject _enemy;
    [SerializeField] private Transform _enemiesRoot;
    [SerializeField] [Range(0, 120)] private float _secondsBetweenSpawn;
    [SerializeField] private Text _wavesCountText;
    [SerializeField] private AudioClip _spawnSFX;

    private int _wavesCount = 0;

    // Use this for initialization
    void Start () {
        StartCoroutine(SpawnEnemies());
        UpdateUI();
    }
    
    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            GetComponent<AudioSource>().PlayOneShot(_spawnSFX);
            Instantiate(_enemy, transform.position, Quaternion.identity, _enemiesRoot);
            _wavesCount++;
            UpdateUI();
            yield return new WaitForSeconds(_secondsBetweenSpawn);
        }
    }

    private void UpdateUI()
    {
        _wavesCountText.text = _wavesCount.ToString();
    }
}
