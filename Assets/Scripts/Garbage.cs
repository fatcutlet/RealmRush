﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Garbage : MonoBehaviour {
    [SerializeField] private float _lifetime = 1f;

    // Use this for initialization
    void Start () {
        Invoke("CollectGarbage", _lifetime);
    }
    
    private void CollectGarbage()
    {
        Destroy(gameObject);
    }
}
