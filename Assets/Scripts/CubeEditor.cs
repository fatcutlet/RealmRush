﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour {

    private Waypoint _waypoint;

    private void Awake()
    {
        _waypoint = GetComponent<Waypoint>();
    }

    private void Update()
    {
        SnapToGrid();
        UpdateLabel();
    }

    private void UpdateLabel()
    {
        var gridSize = _waypoint.GridSize;
        var textMesh = GetComponentInChildren<TextMesh>();

        var gridPos = _waypoint.GetGridPosition();
        var labelText = gridPos.x + "," + gridPos.y;
        textMesh.text = labelText;
        gameObject.name = labelText;
    }

    private void SnapToGrid()
    {
        var gridPos = _waypoint.GetGridPosition() * _waypoint.GridSize;
        transform.localPosition = new Vector3(gridPos.x, 0f, gridPos.y);
    }
}
